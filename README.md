# SSLS (*Stupid Simple Link Saver*) #
A simple Android application to save web URLs.


## Learning Experiences ##
In developing this project, I learned about and used these:

* SQLiteOpenHelper and the SQL Lite DB

* android.util.Patterns and matching

* Intent filters and accepting intents from other apps

* Restricting an activity to only have one instance at a time (*android:launchMode="singleTask"*)


##Screenshots##
###Main Screen###
![Main Screen](https://bitbucket.org/repo/ezezKE/images/508771354-main.png)

###Blank Add Link screen###
![Blank Add Link screen](https://bitbucket.org/repo/ezezKE/images/3640825382-addLinkBlank.png)

###System wide intent for a URL from the web browser###
![System wide intent for a URL from the web browser](https://bitbucket.org/repo/ezezKE/images/1601345044-intent.png)

###Add Link screen opened from web browser intent###
![Add Link screen opened from web browser intent](https://bitbucket.org/repo/ezezKE/images/849999737-addLinkIntent.png)

###Delete confirmation dialog###
![Delete confirmation dialog](https://bitbucket.org/repo/ezezKE/images/239868509-delete.png)
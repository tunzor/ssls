package com.tunzor.ssls;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ListView linksListView = null;
    DBHelper db = new DBHelper(this);
    FragmentManager fm = getSupportFragmentManager();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Bundle b = getIntent().getExtras();
        if (b != null) {
            if(b.getString("linkAdded") != null) {
                String message = b.getString("linkAdded");
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }

        linksListView = (ListView) findViewById(R.id.mainLinksListView);

        showLinksList();

        linksListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Link link  = (Link) parent.getItemAtPosition(position);
                String url = link.getUrl();
                Intent in = new Intent(Intent.ACTION_VIEW);
                in.setData(Uri.parse(url));
                startActivity(in);
            }
        });
        linksListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Link link = (Link) parent.getItemAtPosition(position);
                DeleteLinkDialogFragment deleteFrag = new DeleteLinkDialogFragment();
                deleteFrag.setLink(link);
                if(!fm.isDestroyed()) {
                    deleteFrag.show(fm, "");
                    return true;
                } else {
                    return false;
                }
            }
        });


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(getApplicationContext(), AddLink.class);
                startActivity(in);
            }
        });
    }
    public void showLinksList() {
        ListView lv;
        ArrayList linksList = db.getLinks();
        if(linksList.isEmpty()) {
            lv = (ListView)findViewById(linksListView.getId());
            TextView emptyText = (TextView) findViewById(R.id.emptyList);
            lv.setEmptyView(emptyText);
        }
        LinksAdapter adapter = new LinksAdapter(getApplicationContext(), linksList);
        lv = (ListView)findViewById(linksListView.getId());
        lv.setAdapter(adapter);
    }
    //TODO Place shortcut on home during install
    // http://stackoverflow.com/questions/4854197/how-can-i-place-app-icon-on-launcher-home-screen

    @Override
    public void onResume() {
        super.onResume();
        showLinksList();
    }
}

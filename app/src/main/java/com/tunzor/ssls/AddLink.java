package com.tunzor.ssls;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by Anthony on 1/17/2017.
 */

public class AddLink extends AppCompatActivity {

    Button addButton;
    EditText linkName, linkUrl;
    DBHelper db = new DBHelper(this);
    Link link;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_link_activity);
        addButton = (Button) findViewById(R.id.addLinkButton);
        linkName = (EditText) findViewById(R.id.addLinkName);
        linkUrl = (EditText) findViewById(R.id.addLinkUrl);

        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();

        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if ("text/plain".equals(type)) {
                String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
                if (sharedText != null) {
                    linkUrl.setText(sharedText);
                }
            }
        }

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                link = new Link(linkName.getText().toString(), linkUrl.getText().toString());

                if(link.isEmpty()) {
                    Toast.makeText(AddLink.this, "Name and URL fields cannot be empty.", Toast.LENGTH_SHORT).show();
                } else if(!validURL(link.getUrl())) {
                    Toast.makeText(AddLink.this, "URL entered is invalid.", Toast.LENGTH_SHORT).show();
                } else if(db.dupLink(link)) {
                    Toast.makeText(AddLink.this, "There's already an entry with the name/URL.", Toast.LENGTH_SHORT).show();
                } else if(db.insertLink(link)) {
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("linkAdded", "Added URL with name "+ link.getName());
                    link = new Link();
                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(AddLink.this, "Oops, something else went wrong...", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public boolean validURL(String url) {
        if(!url.startsWith("http://") && !url.startsWith("https://")) {
            link.setUrl("http://" + url);
            url = "http://" + url;
        }
        return Patterns.WEB_URL.matcher(url).matches();
    }
}
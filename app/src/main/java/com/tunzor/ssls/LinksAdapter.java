package com.tunzor.ssls;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Anthony on 1/17/2017.
 */

public class LinksAdapter extends BaseAdapter {

    Context thisContext;
    ArrayList<Link> linksArray;

    public LinksAdapter (Context context, ArrayList<Link> links) {
        this.thisContext = context;
        this.linksArray = links;
    }
    @Override
    public int getCount() {
        if(linksArray.size() == 0) {
            return 0;
        } else {
            return linksArray.size();
        }
    }

    @Override
    public Link getItem(int position) {
        if(position >= 0 && position < linksArray.size()) {
            return linksArray.get(position);
        } else {
            return null;
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Link link = getItem(position);
        View view;

        if(convertView == null) {
            view = LayoutInflater.from(thisContext).inflate(R.layout.list_item_links, parent, false);
        } else {
            view = convertView;
        }

        TextView tv_link_name = (TextView) view.findViewById(R.id.link_name);
        tv_link_name.setText(link.getName());

        TextView tv_link_url = (TextView) view.findViewById(R.id.link_url);
        tv_link_url.setText(link.getUrl());

        return view;
    }
}

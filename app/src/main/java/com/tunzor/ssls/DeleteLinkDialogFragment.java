package com.tunzor.ssls;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

/**
 * Created by Anthony on 2/12/2017.
 */

public class DeleteLinkDialogFragment extends DialogFragment {
    View view;
    MainActivity main;
    DBHelper db;
    Link link;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if(getActivity() != null) {
            db = new DBHelper(getContext());
        }
        main = (MainActivity) getContext();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.delete_link_dialog);
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(deleteLink(link)) {
                    main.showLinksList();
                    Toast.makeText(main, "Deleted "+link.getName(), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(main, "Error. Link not deleted.", Toast.LENGTH_SHORT).show();
                }
            }
        });
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        setCancelable(false);
        return builder.create();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().setTitle("Delete Link");
        return view;
    }

    private boolean deleteLink(Link link) {
        return db.deleteLink(link.getId());
    }

    public void setLink(Link link) {
        this.link = link;
    }
}

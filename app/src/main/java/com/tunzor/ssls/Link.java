package com.tunzor.ssls;

/**
 * Created by Anthony on 1/16/2017.
 */

public class Link {

    private String name;
    private String url;
    private int id;

    public Link() {
        name = "";
        url = "";
    }

    public Link(String n, String u) {
        name = n.trim();
        url = u.trim();
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isEmpty() {
        if ("".equalsIgnoreCase(name) || "".equalsIgnoreCase(url)) {
            return true;
        } else {
            return false;
        }
    }
}

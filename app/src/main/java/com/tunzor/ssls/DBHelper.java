package com.tunzor.ssls;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

/**
 * Created by Anthony on 1/16/2017.
 */

public class DBHelper extends SQLiteOpenHelper {

    public DBHelper(Context context) {
        super(context, "SSLS_DB", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE links"+
                "(_id INTEGER primary key, name TEXT, url TEXT);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP IF EXISTS TABLE links");
        onCreate(db);
    }

    public boolean insertLink(Link link) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("name", link.getName());
        values.put("url", link.getUrl());

        return db.insert("links", null, values) != -1;
    }

    public ArrayList<Link> getLinks() {
        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList list = new ArrayList();
        Cursor res = db.rawQuery("SELECT * FROM links ORDER BY name ASC", null);
        if(res != null && res.moveToFirst()) {
            while (!res.isAfterLast()) {
                Link tempLink = new Link();
                tempLink.setName(res.getString(res.getColumnIndex("name")));
                tempLink.setUrl(res.getString(res.getColumnIndex("url")));
                tempLink.setId(res.getInt(res.getColumnIndex("_id")));
                list.add(tempLink);
                res.moveToNext();
            }
        }
        res.close();
        return list;
    }

    public boolean dupLink(Link link) {
        ArrayList<Link> links = getLinks();
        if(links.size() > 0) {
            for (int i = 0; i < links.size(); i++) {
                if (link.getName().equalsIgnoreCase(links.get(i).getName())
                        || link.getUrl().equalsIgnoreCase(links.get(i).getUrl())) {
                    return true;
                }
            }
        }
        return false;
    }
    public boolean deleteLink(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        String linkId = Integer.toString(id);
        return db.delete("links", "_id=?", new String[]{linkId}) > 0;
    }
    public Link getLink(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList list = new ArrayList();
        Cursor res = db.rawQuery("SELECT * FROM links WHERE _id="+id, null);
        Link link = null;
        if(res != null && res.moveToFirst()) {
            while (!res.isAfterLast()) {
                link.setName(res.getString(res.getColumnIndex("name")));
                link.setUrl(res.getString(res.getColumnIndex("url")));
                link.setId(res.getInt(res.getColumnIndex("_id")));
                res.moveToNext();
            }
        }
        res.close();
        return link;
    }
}
